import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TPanel extends JPanel {
    public TPanel() {
        setLayout(null);
        JButton btn = new JButton("Print BsTree");
        btn.setBounds(10, 10, 100, 20);
        add(btn);
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int[] ini = {60, 76, 52, 71, 65, 74, 89, 23, 15, 40, 90, 87, 99};
                BsTreeGUI tree = new BsTreeGUI();
                tree.init(ini);
                tree.print();
                tree.printGUI((Graphics2D) getGraphics(), getWidth());
            }
        });
    }
}
