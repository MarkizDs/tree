public class BsMain {
    public static void main(String[] args) {
        int[] ini = {60, 76, 52, 71, 65, 74, 89, 23, 40, 90, 87, 99, 70, 75, 88, 41};
        BsTreeIterator tree = new BsTreeIterator();
        tree.init(ini);
        for (Integer i : tree) {
            System.out.print(i + ", ");
        }
    }
}
