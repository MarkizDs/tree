import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BsTreeTest {
    @Test
    public void testSizeMany() {
        int[] ini = {60, 76, 52, 71, 89, 23, 15, 40, 90, 87, 99};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.size();
        assertEquals(11, rr);
    }

    @Test
    public void testSizeTwo() {
        int[] ini = {60, 76};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.size();
        assertEquals(2, rr);
    }

    @Test
    public void testSizeOne() {
        int[] ini = {60};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.size();
        assertEquals(1, rr);
    }

    @Test
    public void testSizeZero() {
        int[] ini = {};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.size();
        assertEquals(0, rr);
    }

    @Test
    public void testSizeNull() {
        int[] ini = null;
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.size();
        assertEquals(0, rr);
    }

    @Test
    public void testClearMany() {
        int[] ini = {60, 76, 52, 71, 89, 23, 15, 40, 90, 87, 99};
        BsTree tree = new BsTree();
        tree.init(ini);
        tree.clear();
        assertEquals(null, tree.root);
    }

    @Test
    public void testClearTwo() {
        int[] ini = {60, 76};
        BsTree tree = new BsTree();
        tree.init(ini);
        tree.clear();
        assertEquals(null, tree.root);
    }

    @Test
    public void testClearOne() {
        int[] ini = {60};
        BsTree tree = new BsTree();
        tree.init(ini);
        tree.clear();
        assertEquals(null, tree.root);
    }

    @Test
    public void testClearZero() {
        int[] ini = {};
        BsTree tree = new BsTree();
        tree.init(ini);
        tree.clear();
        assertEquals(null, tree.root);
    }

    @Test
    public void testClearNull() {
        int[] ini = null;
        BsTree tree = new BsTree();
        tree.init(ini);
        tree.clear();
        assertEquals(null, tree.root);
    }

    @Test
    public void testAddMany() {
        int[] ini = {60, 76, 52, 71, 89, 23, 15, 40, 90, 87, 99};
        BsTree tree = new BsTree();
        tree.init(ini);
        tree.add(10);
        int rr = tree.size();
        assertEquals(12, rr);
    }

    @Test
    public void testAddTwo() {
        int[] ini = {60, 76};
        BsTree tree = new BsTree();
        tree.init(ini);
        tree.add(10);
        int rr = tree.size();
        assertEquals(3, rr);
    }

    @Test
    public void testAddOne() {
        int[] ini = {60};
        BsTree tree = new BsTree();
        tree.init(ini);
        tree.add(10);
        int rr = tree.size();
        assertEquals(2, rr);
    }

    @Test
    public void testAddZero() {
        int[] ini = {};
        BsTree tree = new BsTree();
        tree.init(ini);
        tree.add(10);
        int rr = tree.size();
        assertEquals(1, rr);
    }

    @Test
    public void testAddNull() {
        int[] ini = null;
        BsTree tree = new BsTree();
        tree.init(ini);
        tree.add(10);
        int rr = tree.size();
        assertEquals(1, rr);
    }

    @Test
    public void testLeavesMany() {
        int[] ini = {60, 76, 52, 71, 89, 23, 15, 40, 90, 87, 99};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.leaves();
        assertEquals(5, rr);
    }

    @Test
    public void testLeavesTwo() {
        int[] ini = {60, 76};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.leaves();
        assertEquals(1, rr);
    }

    @Test
    public void testLeavesOne() {
        int[] ini = {60};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.leaves();
        assertEquals(1, rr);
    }

    @Test
    public void testLeavesZero() {
        int[] ini = {};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.leaves();
        assertEquals(0, rr);
    }

    @Test
    public void testLeavesNull() {
        int[] ini = {};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.leaves();
        assertEquals(0, rr);
    }

    @Test
    public void testNodesMany() {
        int[] ini = {60, 76, 52, 71, 89, 23, 15, 40, 90, 87, 99};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.nodes();
        assertEquals(6, rr);
    }

    @Test
    public void testNodesNull() {
        int[] ini = null;
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.nodes();
        assertEquals(0, rr);
    }

    @Test
    public void testNodesTwo() {
        int[] ini = {60, 76};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.nodes();
        assertEquals(1, rr);
    }

    @Test
    public void testNodesOne() {
        int[] ini = {60};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.nodes();
        assertEquals(0, rr);
    }

    @Test
    public void testNodesZero() {
        int[] ini = {};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.nodes();
        assertEquals(0, rr);
    }

    @Test
    public void testHeightMany() {
        int[] ini = {60, 76, 52, 71, 89, 23, 15, 40, 90, 87, 99};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.height();
        assertEquals(5, rr);
    }

    @Test
    public void testHeightTwo() {
        int[] ini = {60, 76};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.height();
        assertEquals(2, rr);
    }

    @Test
    public void testHeightOne() {
        int[] ini = {60};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.height();
        assertEquals(1, rr);
    }

    @Test
    public void testHeightZero() {
        int[] ini = {};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.height();
        assertEquals(0, rr);
    }

    @Test
    public void testHeightNull() {
        int[] ini = null;
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.nodes();
        assertEquals(0, rr);
    }

    @Test
    public void testWidthMany() {
        int[] ini = {60, 76, 52, 71, 89, 23, 15, 40, 90, 87, 99};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.width();
        assertEquals(4, rr);
    }

    @Test
    public void testWidthTwo() {
        int[] ini = {60, 76};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.width();
        assertEquals(1, rr);
    }

    @Test
    public void testWidthOne() {
        int[] ini = {60};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.width();
        assertEquals(1, rr);
    }

    @Test
    public void testWidthZero() {
        int[] ini = {};
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.width();
        assertEquals(0, rr);
    }

    @Test
    public void testWidthNull() {
        int[] ini = null;
        BsTree tree = new BsTree();
        tree.init(ini);
        int rr = tree.width();
        assertEquals(0, rr);
    }

    @Test
    public void testReverseMany() {
        int[] ini = {60, 76, 52, 71, 89, 23, 15, 40, 90, 87, 99};
        BsTree tree = new BsTree();
        tree.init(ini);
        tree.reverse();
        String rr = tree.toString();
        assertEquals("99 90 89 87 76 71 60 52 40 23 15 ", rr);
    }

    @Test
    public void testReverseTwo() {
        int[] ini = {60, 76};
        BsTree tree = new BsTree();
        tree.init(ini);
        tree.reverse();
        String rr = tree.toString();
        assertEquals("76 60 ", rr);
    }

    @Test
    public void testReverseOne() {
        int[] ini = {60};
        BsTree tree = new BsTree();
        tree.init(ini);
        tree.reverse();
        String rr = tree.toString();
        assertEquals("60 ", rr);
    }

    @Test
    public void testReverseZero() {
        int[] ini = {};
        BsTree tree = new BsTree();
        tree.init(ini);
        tree.reverse();
        String rr = tree.toString();
        assertEquals("", rr);
    }
}
