import java.awt.Graphics2D;

public class BsTreeGUI extends BsTree {
    public void printGUI(Graphics2D gg, int right) {
        printNodeGUI(root, gg, 0, right, 1, 80, right / 2, 0);
    }

    private void printNodeGUI(BsTree.Node p, Graphics2D gg, int left, int right, int level, int dy, int px, int py) {
        if (p == null)
            return;

        int x = (left + right) / 2;
        int y = dy * level;

        gg.drawString("" + p.val, x - 7, y + 20);
        gg.drawOval(x - 15, y, 30, 30);
        gg.drawLine(x, y, px, py + 30);

        System.out.print(p.val + ",");
        printNodeGUI(p.left, gg, left, x, level + 1, dy, x, y);
        printNodeGUI(p.right, gg, x, right, level + 1, dy, x, y);
    }
}
