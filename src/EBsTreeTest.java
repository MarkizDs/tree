import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class EBsTreeTest {

    EBsTree obj = null;

    @Parameters
    public static Collection<Object[]> EBsTree() {
        return Arrays.asList(new Object[][]
                {
                        {new BsTree()},
                        {new BsTreeIterator()},
                });
    }

    public EBsTreeTest(EBsTree Paramet) {
        obj = Paramet;
    }

    @Before
    public void start() {
        obj.clear();
    }

    @Test
    public void testToStringMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        obj.init(ini);
        String act = obj.toString();
        String exp = "10, 11, 20, 24, 77, 82, ";
        assertEquals(exp, act);
    }

    @Test
    public void testToStringTwo() {
        int[] ini = {10, 20};
        obj.init(ini);
        String act = obj.toString();
        String exp = "10, 20, ";
        assertEquals(exp, act);
    }

    @Test
    public void testToStringOne() {
        int[] ini = {10};
        obj.init(ini);
        String act = obj.toString();
        String exp = "10, ";
        assertEquals(exp, act);
    }

    @Test
    public void testToStringEmpty() {
        int[] ini = {};
        obj.init(ini);
        String act = obj.toString();
        String exp = "";
        assertEquals(exp, act);
    }

    @Test
    public void testToArrayManyOne() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {10, 11, 20, 24, 77, 82};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testToArrayManyTwo() {
        int[] ini = {50, 25, 40, 20, 11, 24, 30, 75, 100, 60, 45, 22, 43, 77};
        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {11, 20, 22, 24, 25, 30, 40, 43, 45, 50, 60, 75, 77, 100};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testToArrayTwo() {
        int[] ini = {10, 20};
        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {10, 20};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testToArrayOne() {
        int[] ini = {10};
        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testToArrayEmpty() {
        int[] ini = {};
        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testInitMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {10, 11, 20, 24, 77, 82};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testInitTwo() {
        int[] ini = {10, 20};
        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {10, 20};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testInitOne() {
        int[] ini = {10};
        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {10};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testInitEmpty() {
        int[] ini = {};
        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testInitNull() {
        int[] ini = null;
        obj.init(ini);
        int[] act = obj.toArray();
        int[] exp = {};
        Assert.assertEquals(0, obj.size());
        assertArrayEquals(exp, act);
    }

    @Test
    public void testSizeMany() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        obj.init(ini);
        Assert.assertEquals(6, obj.size());
    }

    @Test
    public void testSizeTwo() {
        int[] ini = {10, 20};
        obj.init(ini);
        Assert.assertEquals(2, obj.size());
    }

    @Test
    public void testSizeOne() {
        int[] ini = {10};
        obj.init(ini);
        Assert.assertEquals(1, obj.size());
    }

    @Test
    public void testSizeEmpty() {
        int[] ini = {};
        obj.init(ini);
        Assert.assertEquals(0, obj.size());
    }

    @Test
    public void testLeavesManyOne() {
        int[] ini = {50, 25, 40, 20, 11, 24, 30, 75, 100, 60, 45};
        obj.init(ini);
        Assert.assertEquals(6, obj.leaves());
    }

    @Test
    public void testLeavesManyTwo() {
        int[] ini = {50, 25, 40, 20, 11, 24, 30, 75, 100, 60, 45, 22, 43, 77};
        obj.init(ini);
        Assert.assertEquals(6, obj.leaves());
    }

    @Test
    public void testLeavesTwo() {
        int[] ini = {50, 25};
        obj.init(ini);
        Assert.assertEquals(1, obj.leaves());
    }

    @Test
    public void testLeavesOne() {
        int[] ini = {50};
        obj.init(ini);
        Assert.assertEquals(1, obj.leaves());
    }

    @Test
    public void testLeavesEmpty() {
        int[] ini = {};
        obj.init(ini);
        Assert.assertEquals(0, obj.leaves());
    }

    @Test
    public void testNodesManyOne() {
        int[] ini = {50, 25, 40, 20, 11, 24, 30, 75, 100, 60, 45, 22, 43, 77};
        obj.init(ini);
        Assert.assertEquals(8, obj.nodes());
    }

    @Test
    public void testNodesManyTwo() {
        int[] ini = {50, 25, 40, 20, 11, 24, 30, 75, 100, 60, 45, 47};
        obj.init(ini);
        Assert.assertEquals(6, obj.nodes());
    }

    @Test
    public void testNodesTwo() {
        int[] ini = {50, 25};
        obj.init(ini);
        Assert.assertEquals(1, obj.nodes());
    }

    @Test
    public void testNodesOne() {
        int[] ini = {50};
        obj.init(ini);
        Assert.assertEquals(0, obj.nodes());
    }

    @Test
    public void testNodesEmpty() {
        int[] ini = {};
        obj.init(ini);
        Assert.assertEquals(0, obj.nodes());
    }

    @Test
    public void testClearManyOne() {
        int[] ini = {50, 25, 40, 20, 11, 24, 30, 75, 100, 60, 45, 22, 43, 77};
        obj.init(ini);
        obj.clear();
        int[] exp = {};
        int[] act = obj.toArray();
        Assert.assertEquals(0, obj.size());
        assertArrayEquals(exp, act);
    }

    @Test
    public void testClearTwo() {
        int[] ini = {50, 25};
        obj.init(ini);
        obj.clear();
        int[] exp = {};
        int[] act = obj.toArray();
        Assert.assertEquals(0, obj.size());
        assertArrayEquals(exp, act);
    }

    @Test
    public void testClearOne() {
        int[] ini = {50};
        obj.init(ini);
        obj.clear();
        int[] exp = {};
        int[] act = obj.toArray();
        Assert.assertEquals(0, obj.size());
        assertArrayEquals(exp, act);
    }

    @Test
    public void testClearEmpty() {
        int[] ini = {};
        obj.init(ini);
        obj.clear();
        int[] exp = {};
        int[] act = obj.toArray();
        Assert.assertEquals(0, obj.size());
        assertArrayEquals(exp, act);
    }

    @Test
    public void testHeightManyOne() {
        int[] ini = {50, 25, 40, 20, 11, 24, 30, 75, 100, 60, 45, 22, 43, 77};
        obj.init(ini);
        Assert.assertEquals(5, obj.height());
    }

    @Test
    public void testHeightManyTwo() {
        int[] ini = {50, 25, 40, 20, 11, 24, 30, 75, 100, 60, 45};
        obj.init(ini);
        Assert.assertEquals(4, obj.height());
    }

    @Test
    public void testHeightTwo() {
        int[] ini = {50, 25};
        obj.init(ini);
        Assert.assertEquals(2, obj.height());
    }

    @Test
    public void testHeightOne() {
        int[] ini = {50};
        obj.init(ini);
        Assert.assertEquals(1, obj.height());
    }

    @Test
    public void testHeightEmpty() {
        int[] ini = {};
        obj.init(ini);
        Assert.assertEquals(0, obj.height());
    }

    @Test
    public void testWidthManyOne() {
        int[] ini = {50, 25, 40, 20, 11, 24, 30, 75, 100, 60, 45, 22, 43, 77};
        obj.init(ini);
        Assert.assertEquals(5, obj.width());
    }

    @Test
    public void testWidthManyTwo() {
        int[] ini = {50, 25, 40, 20, 11, 24, 30, 75, 100, 60, 45};
        obj.init(ini);
        Assert.assertEquals(4, obj.width());
    }

    @Test
    public void testWidthTwo() {
        int[] ini = {50, 25};
        obj.init(ini);
        Assert.assertEquals(1, obj.width());
    }

    @Test
    public void testWidthOne() {
        int[] ini = {50};
        obj.init(ini);
        Assert.assertEquals(1, obj.width());
    }

    @Test
    public void testWidthEmpty() {
        int[] ini = {};
        obj.init(ini);
        Assert.assertEquals(0, obj.width());
    }

    @Test
    public void testAddManyOne() {
        int[] ini = {50, 25, 40, 20, 11, 24, 30, 75, 100, 60, 45, 22, 43, 77};
        obj.init(ini);
        obj.add(66);
        int[] act = obj.toArray();
        int[] exp = {11, 20, 22, 24, 25, 30, 40, 43, 45, 50, 60, 66, 75, 77, 100};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddManyTwo() {
        int[] ini = {10, 20, 77, 11, 24, 82};
        obj.init(ini);
        obj.add(100);
        int[] act = obj.toArray();
        int[] exp = {10, 11, 20, 24, 77, 82, 100};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddTwo() {
        int[] ini = {20, 77};
        obj.init(ini);
        obj.add(50);
        int[] act = obj.toArray();
        int[] exp = {20, 50, 77};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddOne() {
        int[] ini = {20};
        obj.init(ini);
        obj.add(50);
        int[] act = obj.toArray();
        int[] exp = {20, 50};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testAddEmpty() {
        int[] ini = {};
        obj.init(ini);
        obj.add(50);
        int[] act = obj.toArray();
        int[] exp = {50};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelManyOne() {
        int[] ini = {50, 25, 40, 20, 11, 24, 30, 66, 75, 100, 60, 45, 22, 43, 77};
        obj.init(ini);
        obj.del(66);
        int[] act = obj.toArray();
        int[] exp = {11, 20, 22, 24, 25, 30, 40, 43, 45, 50, 60, 75, 77, 100};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelManyTwo() {
        int[] ini = {10, 20, 77, 11, 100, 24, 82};
        obj.init(ini);
        obj.del(100);
        int[] act = obj.toArray();
        int[] exp = {10, 11, 20, 24, 77, 82};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelTwo() {
        int[] ini = {20, 77};
        obj.init(ini);
        obj.del(20);
        int[] act = obj.toArray();
        int[] exp = {77};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelOne() {
        int[] ini = {20};
        obj.init(ini);
        obj.del(20);
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testDelEmpty() {
        int[] ini = {};
        obj.init(ini);
        obj.del(50);
        int[] act = obj.toArray();
        int[] exp = {50};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testReverseManyOne() {
        int[] ini = {50, 25, 40, 20, 11, 24, 30, 66, 75, 100, 60, 45, 22, 43, 77};
        obj.init(ini);
        obj.reverse();
        int[] act = obj.toArray();
        int[] exp = {100, 77, 75, 66, 60, 50, 45, 43, 40, 30, 25, 24, 22, 20, 11};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testReverseManyTwo() {
        int[] ini = {10, 20, 77, 11, 100, 24, 82};
        obj.init(ini);
        obj.reverse();
        int[] act = obj.toArray();
        int[] exp = {10, 11, 20, 24, 77, 82, 100};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testReverseTwo() {
        int[] ini = {20, 77};
        obj.init(ini);
        obj.reverse();
        int[] act = obj.toArray();
        int[] exp = {20, 77};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testReverseOne() {
        int[] ini = {20};
        obj.init(ini);
        obj.reverse();
        int[] act = obj.toArray();
        int[] exp = {20};
        assertArrayEquals(exp, act);
    }

    @Test
    public void testReverseEmpty() {
        int[] ini = {};
        obj.init(ini);
        obj.reverse();
        int[] act = obj.toArray();
        int[] exp = {};
        assertArrayEquals(exp, act);
    }
}
